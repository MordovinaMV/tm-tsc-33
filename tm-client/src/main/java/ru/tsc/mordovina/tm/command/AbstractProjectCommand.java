package ru.tsc.mordovina.tm.command;

import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.endpoint.Project;
import ru.tsc.mordovina.tm.exception.entity.ProjectNotFoundException;

import java.util.Optional;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(@Nullable final Project project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
    }

}

package ru.tsc.mordovina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.command.AbstractProjectCommand;
import ru.tsc.mordovina.tm.endpoint.Project;
import ru.tsc.mordovina.tm.endpoint.Role;
import ru.tsc.mordovina.tm.endpoint.Session;

import java.util.List;

public final class ProjectListShowCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getCommand() {
        return "project-list";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final List<Project> projects;
        projects = serviceLocator.getProjectEndpoint().findProjectAll(session);
        for (final Project project : projects) showProject(project);
    }

}

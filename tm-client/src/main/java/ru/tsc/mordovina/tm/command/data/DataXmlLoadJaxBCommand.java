package ru.tsc.mordovina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.endpoint.Role;
import ru.tsc.mordovina.tm.endpoint.Session;

public class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String getCommand() {
        return "data-xml-jaxb-load";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public String getDescription() {
        return "Load xml data from file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final Session session = serviceLocator.getSession();
        serviceLocator.getAdminDataEndpoint().loadDataXmlJaxB(session);
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}

package ru.tsc.mordovina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.command.AbstractProjectCommand;
import ru.tsc.mordovina.tm.endpoint.Role;
import ru.tsc.mordovina.tm.endpoint.Session;
import ru.tsc.mordovina.tm.util.TerminalUtil;

public final class ProjectFinishByIdCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getCommand() {
        return "project-finish-by-id";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Finish project by id";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSession();
        System.out.println("Enter id");
        @Nullable final String id = TerminalUtil.nextLine();
        serviceLocator.getProjectEndpoint().finishProjectById(session, id);
    }

}

package ru.tsc.mordovina.tm.exception.empty;

import ru.tsc.mordovina.tm.exception.AbstractException;

public class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("Error. Email is empty.");
    }
}

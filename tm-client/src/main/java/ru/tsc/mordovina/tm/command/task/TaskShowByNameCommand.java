package ru.tsc.mordovina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.command.AbstractTaskCommand;
import ru.tsc.mordovina.tm.endpoint.Role;
import ru.tsc.mordovina.tm.endpoint.Session;
import ru.tsc.mordovina.tm.endpoint.Task;
import ru.tsc.mordovina.tm.util.TerminalUtil;

public final class TaskShowByNameCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getCommand() {
        return "task-show-by-name";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show task by name";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSession();
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        @NotNull final Task task = serviceLocator.getTaskEndpoint().findTaskByName(session, name);
        showTask(task);
    }

}

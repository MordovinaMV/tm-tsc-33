package ru.tsc.mordovina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.command.AbstractTaskCommand;
import ru.tsc.mordovina.tm.endpoint.Project;
import ru.tsc.mordovina.tm.endpoint.Role;
import ru.tsc.mordovina.tm.endpoint.Session;
import ru.tsc.mordovina.tm.util.TerminalUtil;

public final class TaskAddToProjectByIdProjectTaskCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getCommand() {
        return "task-add-to-project-by-id";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Add task to project by id";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSession();
        System.out.println("Enter project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectEndpoint().findProjectById(session, projectId);
        System.out.println("Enter task id");
        @NotNull final String taskId = TerminalUtil.nextLine();
        serviceLocator.getTaskEndpoint().bindTaskById(session, projectId, taskId);
    }

}

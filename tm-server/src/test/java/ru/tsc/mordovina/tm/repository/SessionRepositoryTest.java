package ru.tsc.mordovina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.mordovina.tm.api.repository.ISessionRepository;
import ru.tsc.mordovina.tm.exception.AbstractException;
import ru.tsc.mordovina.tm.model.Session;
import ru.tsc.mordovina.tm.model.User;

public class SessionRepositoryTest {

    @NotNull
    private final ISessionRepository sessionRepository;

    @NotNull
    private final Session session;

    @NotNull
    private final String sessionId;

    public SessionRepositoryTest() {
        sessionRepository = new SessionRepository();
        session = new Session();
        sessionId = session.getId();
    }

    @Before
    public void initializeTest() {
        session.setUserId(new User().getId());
    }

    @Test
    public void openClose() throws AbstractException {
        sessionRepository.add(session);
        Assert.assertEquals(session, sessionRepository.findById(sessionId));
        sessionRepository.remove(session);
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }

    @Test
    public void contains() {
        sessionRepository.add(session);
        Assert.assertTrue(sessionRepository.contains(sessionId));
    }

    @After
    public void finalizeTest() {
        sessionRepository.clear();
    }

}

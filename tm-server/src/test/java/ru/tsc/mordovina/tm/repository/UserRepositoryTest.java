package ru.tsc.mordovina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.mordovina.tm.api.repository.IUserRepository;
import ru.tsc.mordovina.tm.exception.AbstractException;
import ru.tsc.mordovina.tm.model.User;

public class UserRepositoryTest {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final User user;

    @NotNull
    private final String userLogin = "userTest";

    public UserRepositoryTest() {
        userRepository = new UserRepository();
        user = new User();
        user.setLogin(userLogin);
    }

    @Before
    public void initializeTest() throws AbstractException {
        userRepository.add(user);
    }

    @Test
    public void findByLogin() throws AbstractException {
        Assert.assertEquals(user, userRepository.findUserByLogin(userLogin));
    }

    @Test
    public void removeByLogin() throws AbstractException {
        userRepository.removeUserByLogin(userLogin);
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @After
    public void finalizeTest() {
        userRepository.clear();
    }

}

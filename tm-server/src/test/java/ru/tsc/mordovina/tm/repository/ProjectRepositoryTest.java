package ru.tsc.mordovina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.mordovina.tm.api.repository.IProjectRepository;
import ru.tsc.mordovina.tm.enumerated.Status;
import ru.tsc.mordovina.tm.exception.AbstractException;
import ru.tsc.mordovina.tm.model.Project;
import ru.tsc.mordovina.tm.model.User;
import ru.tsc.mordovina.tm.util.HashUtil;

public class ProjectRepositoryTest {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final Project project;

    @NotNull
    private final String projectId;

    @NotNull
    private final String projectName = "testProject";

    @NotNull
    private final String projectDescription = "testProject";

    @NotNull
    private final String userId;

    public ProjectRepositoryTest() {
        projectRepository = new ProjectRepository();
        @NotNull final User user = new User();
        userId = user.getId();
        user.setLogin("guest");
        user.setPassword(HashUtil.salt( "qwe",3, "guest"));
        project = new Project();
        projectId = project.getId();
        project.setUserId(userId);
        project.setName(projectName);
        project.setDescription(projectDescription);
    }

    @Before
    public void initializeTest() throws AbstractException {
        projectRepository.add(project);
    }

    @Test
    public void findProject() throws AbstractException {
        Assert.assertEquals(project, projectRepository.findByName(userId, projectName));
        Assert.assertEquals(project, projectRepository.findById(projectId));
        Assert.assertEquals(project, projectRepository.findById(userId, projectId));
        Assert.assertEquals(project, projectRepository.findByIndex(0));
    }

    @Test
    public void removeByName() throws AbstractException {
        Assert.assertNotNull(project);
        projectRepository.removeByName(userId, projectName);
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @Test
    public void startById() throws AbstractException {
        projectRepository.startById(userId, projectId);
        Assert.assertEquals(projectRepository.findById(userId, projectId).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByIndex() throws AbstractException {
        projectRepository.startByIndex(userId, 0);
        Assert.assertEquals(projectRepository.findByIndex(userId, 0).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByName() throws AbstractException {
        projectRepository.startByName(userId, projectName);
        Assert.assertEquals(projectRepository.findByName(userId, projectName).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void finishById() throws AbstractException {
        projectRepository.finishById(userId, projectId);
        Assert.assertEquals(projectRepository.findById(userId, projectId).getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByIndex() throws AbstractException {
        projectRepository.finishByIndex(userId, 0);
        Assert.assertEquals(projectRepository.findByIndex(userId, 0).getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByName() throws AbstractException {
        projectRepository.finishByName(userId, projectName);
        Assert.assertEquals(projectRepository.findByName(userId, projectName).getStatus(), Status.COMPLETED);
    }

    @Test
    public void updateStatusById() throws AbstractException {
        projectRepository.changeStatusById(userId, projectId, Status.IN_PROGRESS);
        Assert.assertEquals(projectRepository.findById(userId, projectId).getStatus(), Status.IN_PROGRESS);
        projectRepository.changeStatusById(userId, projectId, Status.COMPLETED);
        Assert.assertEquals(projectRepository.findById(userId, projectId).getStatus(), Status.COMPLETED);
        projectRepository.changeStatusById(userId, projectId, Status.NOT_STARTED);
        Assert.assertEquals(projectRepository.findById(userId, projectId).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByIndex() throws AbstractException {
        projectRepository.changeStatusByIndex(userId, 0, Status.IN_PROGRESS);
        Assert.assertEquals(projectRepository.findByIndex(userId, 0).getStatus(), Status.IN_PROGRESS);
        projectRepository.changeStatusByIndex(userId, 0, Status.COMPLETED);
        Assert.assertEquals(projectRepository.findByIndex(userId, 0).getStatus(), Status.COMPLETED);
        projectRepository.changeStatusByIndex(userId, 0, Status.NOT_STARTED);
        Assert.assertEquals(projectRepository.findByIndex(userId, 0).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByName() throws AbstractException {
        projectRepository.changeStatusByName(userId, projectName, Status.IN_PROGRESS);
        Assert.assertEquals(projectRepository.findByName(userId, projectName).getStatus(), Status.IN_PROGRESS);
        projectRepository.changeStatusByName(userId, projectName, Status.COMPLETED);
        Assert.assertEquals(projectRepository.findByName(userId, projectName).getStatus(), Status.COMPLETED);
        projectRepository.changeStatusByName(userId, projectName, Status.NOT_STARTED);
        Assert.assertEquals(projectRepository.findByName(userId, projectName).getStatus(), Status.NOT_STARTED);
    }

    @After
    public void finalizeTest() {
        projectRepository.clear(userId);
    }

}

package ru.tsc.mordovina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.mordovina.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {

    @NotNull
    List<Task> findAllTaskByProjectId(String userId, String projectId);

    void bindTaskToProjectById(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    void unbindTaskById(@NotNull String userId, @NotNull String taskId);

    void removeAllTaskByProjectId(@NotNull String userId, @NotNull String projectId);

}
package ru.tsc.mordovina.tm.api.entity;

public interface IWBS extends IHasCreated, IHasStartDate, IHasName, IHasStatus {
}

package ru.tsc.mordovina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.api.repository.ITaskRepository;
import ru.tsc.mordovina.tm.enumerated.Status;
import ru.tsc.mordovina.tm.exception.empty.EmptyDescriptionException;
import ru.tsc.mordovina.tm.exception.empty.EmptyNameException;
import ru.tsc.mordovina.tm.exception.empty.EmptyUserIdException;
import ru.tsc.mordovina.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public List<Task> findAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return findAll(userId).stream()
                .filter(t -> t.getProjectId().equals(projectId))
                .collect(Collectors.toList());
    }

    @Override
    public void bindTaskToProjectById(@NotNull final String userId, @NotNull final String projectId,
                                      @NotNull final String taskId) {
        @NotNull final Task task = findById(taskId);
        task.setProjectId(projectId);
        task.setUserId(userId);
    }

    @Override
    public void unbindTaskById(@NotNull final String userId, @NotNull final String taskId) {
        @NotNull final Task task = findById(taskId);
        task.setUserId(null);
        task.setProjectId(null);
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        findAll(userId).stream()
                .filter(t -> projectId.equals(t.getProjectId()))
                .forEach(t -> t.setProjectId(null));
    }



}

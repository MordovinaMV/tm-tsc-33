package ru.tsc.mordovina.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.mordovina.tm.enumerated.Role;
import ru.tsc.mordovina.tm.model.Session;
import ru.tsc.mordovina.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {

    @WebMethod
    void updateUserPassword(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "password", partName = "password") final String password
    );

}

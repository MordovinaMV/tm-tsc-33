package ru.tsc.mordovina.tm.api.entity;

import ru.tsc.mordovina.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}

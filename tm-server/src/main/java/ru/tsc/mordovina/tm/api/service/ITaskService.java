package ru.tsc.mordovina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.enumerated.Status;
import ru.tsc.mordovina.tm.model.Session;
import ru.tsc.mordovina.tm.model.Task;

import javax.jws.WebParam;
import java.util.List;

public interface ITaskService extends IOwnerService<Task> {

}

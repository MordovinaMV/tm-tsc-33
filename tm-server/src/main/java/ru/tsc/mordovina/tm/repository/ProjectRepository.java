package ru.tsc.mordovina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.api.repository.IProjectRepository;
import ru.tsc.mordovina.tm.enumerated.Status;
import ru.tsc.mordovina.tm.exception.empty.EmptyDescriptionException;
import ru.tsc.mordovina.tm.exception.empty.EmptyNameException;
import ru.tsc.mordovina.tm.exception.empty.EmptyUserIdException;
import ru.tsc.mordovina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.mordovina.tm.model.Project;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

}

package ru.tsc.mordovina.tm.api.repository;

import ru.tsc.mordovina.tm.model.Project;

public interface IProjectRepository extends IOwnerRepository<Project> {

}

package ru.tsc.mordovina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.mordovina.tm.api.IRepository;
import ru.tsc.mordovina.tm.model.Session;

public interface ISessionRepository extends IRepository<Session> {

    boolean contains(@NotNull String id);

}

package ru.tsc.mordovina.tm.api;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

}
